import Modelclasses.Items;
import Modelclasses.Orders;
import com.mongodb.*;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.*;
import org.bson.types.BasicBSONList;
import java.util.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class MongoDB {
    private MongoClientURI mongoConnection;
    private MongoClient mongoClient;

    public void openMongoConn() {
        mongoConnection = new MongoClientURI("mongodb://localhost:" + 27017);
        mongoClient = new MongoClient(mongoConnection);
    }

    public void closeMongoConn() {
        mongoClient.close();
    }

    public void addOrders(Orders o) {
        openMongoConn();
        MongoDatabase database = mongoClient.getDatabase("shopDB");
        MongoCollection<Document> bs_collection = database.getCollection("Orders");
        Document doc = new Document().append("ID", o.getId())
                                     .append("Items", o.getItems());
        bs_collection.insertOne(doc);
        closeMongoConn();
    }

    public void listOrders() {
        openMongoConn();
        MongoDatabase database = mongoClient.getDatabase("shopDB");
        MongoCollection<Document> o_collection = database.getCollection("Orders");
        FindIterable<Document> iterable = o_collection.find();
        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(Document document) {
                System.out.println(document);
            }
        });
    }
}