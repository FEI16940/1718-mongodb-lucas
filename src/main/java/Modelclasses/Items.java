package Modelclasses;

public class Items {
    private String name;
    private int itemID;

    public Items(int itemID, String name) {
        this.itemID = itemID;
        this.name = name;
    }

    public int getID() {
        return itemID;
    }

    public String getName(){
        return name;
    }

    @Override
    public int hashCode() {
        String neu = name + itemID;
        return neu.hashCode();
    }

    @Override
    public String toString() {
        String i = "Item: " + itemID + "Name: " + name;
        return i;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        final Items b = (Items) obj;
        if (this.itemID != b.itemID) {
            return false;
        } else {
            return true;
        }
    }
}