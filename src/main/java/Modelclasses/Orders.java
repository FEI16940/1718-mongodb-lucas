package Modelclasses;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Orders {
    private int id;
    private ArrayList<Integer> items;

    public Orders(ArrayList<Integer> orders, int id) {
        this.items = orders;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public ArrayList<Integer> getItems() {
        return items;
    }

    @Override
    public String toString() {
        String o = "Item: " + items.toString() + ", ID: " + id;
        return o;
    }

    @Override
    public int hashCode() {
        String neu = items.toString() + id;
        return neu.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        final Orders b = (Orders) obj;
        if (this.id != b.id) {
            return false;
        } else {
            return true;
        }
    }
}
