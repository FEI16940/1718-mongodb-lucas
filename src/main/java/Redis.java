import Modelclasses.Items;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.Map;

public class Redis {

    private Jedis jedis;

    public void openRedisConnection() {
        jedis = new Jedis("localhost");
        System.out.println("Connected to RedisDB");
    }

    public void insertItem(Items item) {
        jedis.hset("item", "Name", item.getName());
        jedis.hset("item", "id", String.valueOf(item.getID()));
    }

    public void listBets() {
        String i;
        i = jedis.hget("item", "Name");
        i += " " + jedis.hget("item", "id");
        System.out.println(i);
    }

    public void closeConn() {
        jedis.close();
    }
}
