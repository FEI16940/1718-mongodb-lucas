import Modelclasses.Bet;
import org.junit.Test;

public class TestRedis {
    Redis redis = new Redis();

    @Test
    public void testConn() {
        redis.openRedisConnection();
    }

    @Test
    public void testInsertAndListBet() {
        Bet b = new Bet(34, 1);
        redis.openRedisConnection();
        redis.insertBet(b);
        redis.listBets();
        redis.closeConn();
    }
}
