import Modelclasses.Orders;
import org.junit.Test;

import java.util.ArrayList;

public class TestMongo {

    MongoDB mongo = new MongoDB();

    @Test
    public void testAddBettingSlip() {
        mongo.openMongoConn();
        ArrayList<Integer> Items = new ArrayList();
        Items.add(1);
        Items.add(2);
        Orders o = new Orders(Items, 1);
        mongo.addOrders(o);
        mongo.closeMongoConn();
    }

    @Test
    public void testListBettingSlips() {
        mongo.openMongoConn();
        mongo.listOrders();
        mongo.closeMongoConn();
    }
}
